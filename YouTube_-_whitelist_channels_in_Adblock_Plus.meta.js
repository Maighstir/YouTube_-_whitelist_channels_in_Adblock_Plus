// ==UserScript==
// @name        YouTube - whitelist channels in Adblock Plus
// @namespace   
// @author      Maighstir
// @credits     Eyeo GmbH, Gantt, rimmington,Gingerbread Man, drm31415
// @description Helps whitelist YouTube channels in adblockers
// @include     http://*.youtube.com/*
// @include     https://*.youtube.com/*
// @version     1.7.20210124.1
// @grant       none
// @license     http://creativecommons.org/licenses/by-sa/4.0/
// @downloadURL https://gitlab.com/Maighstir/YouTube_-_whitelist_channels_in_Adblock_Plus/-/raw/master/YouTube_-_whitelist_channels_in_Adblock_Plus.user.js
// ==/UserScript==
