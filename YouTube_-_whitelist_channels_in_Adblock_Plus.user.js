// ==UserScript==
// @name        YouTube - whitelist channels in Adblock Plus
// @namespace   
// @author      Maighstir
// @credits     Eyeo GmbH, Gantt, rimmington,Gingerbread Man, drm31415
// @description Helps whitelist YouTube channels in adblockers
// @include     http://*.youtube.com/*
// @include     https://*.youtube.com/*
// @version     1.7.20210124.1
// @grant       none
// @license     http://creativecommons.org/licenses/by-sa/4.0/
// @downloadURL https://gitlab.com/Maighstir/YouTube_-_whitelist_channels_in_Adblock_Plus/-/raw/master/YouTube_-_whitelist_channels_in_Adblock_Plus.user.js
// ==/UserScript==

var updateHref = function (url) {
    window.history.replaceState(history.state, "", url);
};

var activate = function () {
    if (location.href.search("&user=") != -1) return;

    var AppendFields="";
    var u = document.querySelector('#watch7-content > span[itemprop="author"] > link[itemprop="url"]');
    if (!u) {
        console.warn("COULD NOT FIND CHANNEL NAME. Mind helping me out?")
        console.warn("You can find me at https://gitlab.com/Maighstir/YouTube_-_whitelist_channels_in_Adblock_Plus")
    }
    var channelName = u.href.slice(u.href.lastIndexOf('/')+1)

    if(channelName){
        // No idea how to find subscription status or notification level.
        var SubbedStatus="";
        var NotificationLevel="";

        AppendFields+="?user="+channelName;
    }

    if (AppendFields!="") {
        updateHref(location.href+AppendFields);
    }
}

// For static pages
activate();

// For dynamic content changes, like when clicking a video on the main page.
// This bit is based on Gantt's excellent Download YouTube Videos As MP4 script:
// https://github.com/gantt/downloadyoutube
var observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {
        if (mutation.addedNodes !== null) {
            for (i = 0; i < mutation.addedNodes.length; i++) {
            if (mutation.addedNodes[i].id == "movie_player") {
                activate();
                break;
            }
            }
        }
    });
});
observer.observe(document.body, {childList: true, subtree: true});
