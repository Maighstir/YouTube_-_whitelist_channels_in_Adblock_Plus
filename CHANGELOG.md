#Captain's log, date 2021-01-24 CE
* Didn't work on some channels. Fixed that (as far as I know) with a new single
querySelector.
* Removed the @supportURL UserScript tag. It wasn't a full URL (had been
shortened a few versions ago), and the forum thread it pointed to had been dead
for a few years anyway. Should probably have some place for support, but don't
know where yet.
* Added a warning (to the browser's JavaScript console) if a channel/user name
cannot be found, with the URL to mu repository. Figured that someone looking at
the console to try and figure out why the URL doesn't change might at least
know enough to be dangerous and may want to help.

#Captain's log, date 2020-11-28 CE
I finally got fed up with the old userscript that should allow ads on specific
Youtube channels not working, and figured out a way to patch it up a bit. It's
probably not stable. Have to get engineering to make a better one, who knows
what Google decides to change next, and when?

I cut out a few features from the latest know iteration I can make do without
in order to simplify the code, though I made sure to record and keep a log of
previous owners' modifications.
