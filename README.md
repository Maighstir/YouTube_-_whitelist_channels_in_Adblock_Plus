# YouTube - whitelist channels in Adblock Plus
(It works with other adblockers, too.)

## What?
Should (provided it works as it should...) add the username of a Youtube channel
to the URL string in order for various adblockers to detect it and disable
themselves.

## Why?
The source at
[https://greasyfork.org/en/scripts/4168-youtube-whitelist-channels-in-adblock-plus][s1]
was last updated on 2016-02-02, and no longer works (for me at least). I guess
Youtube has updated the HTML output of their pages in the last five years.

[s1]: https://greasyfork.org/en/scripts/4168-youtube-whitelist-channels-in-adblock-plus

There's been some modifications at the support forum thread at
[https://adblockplus.org/forum/viewtopic.php?f=1&t=23697][s2], but even that
was last updated on 2017-08-06.

[s2]: https://adblockplus.org/forum/viewtopic.php?f=1&t=23697

I updated the userscript to the best of my (barely sufficient) ability for it
to work in late 2020. Now, at least I have a place to continue whenever Google
makes a change that breaks it again.

Most (all?) of the history at the greasyfork.org page and subsequent updates on
the adblockplus.org forums are included with appropriate dates in the git
history here.

## How?
You'll need some way of running so called userscripts in your browser.

* In Pale Moon, I run a fork of GM called [Greasemonkey for Pale Moon][gm4pm]
* For Firefox there's [Greasemonkey][gm]
* For Chrome there's [Tampermonkey][tm]

[gm4pm]: https://github.com/janekptacijarabaci/greasemonkey/releases/
[gm]: https://addons.mozilla.org/en-US/firefox/addon/greasemonkey/
[tm]: https://chrome.google.com/webstore/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo

Some browsers may have support built-in, some may be able to use the above
mentioned addons for their "parent" browsers, and yet others may have their own
add-ons. There may also be other add-ons for the above mentioned browsers that
do the same thing. I don't know what you should use, do your own research.
